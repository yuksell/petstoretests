### Main requirements ###

* Python 3.5 or higher
* [Pytest](https://pytest.org/)
* [AssertPy](https://github.com/assertpy/assertpy)
* IDE - [PyCharm](https://www.jetbrains.com/pycharm/)

### Setup Quickstart ###

* Install Python 3.5 or higher
* Clone this repository
* Open Terminal and enter project's root directory: `$ cd <Project Root>`
* Create virtualenv: `$ python3 -m venv my_venv`
* Activate virtualenv: `$ source ./my_venv/bin/activate`
* Install project dependencies: `(my_venv)$ pip3 install -r ./requirements.txt`

### Run tests in PyCharm ###
* Open project in PyCharm: select 'File' -> 'Open...' and locate the project root folder.
* Test suites are located in `tests/` folder.
* Ensure pytest is the default test runner: go to 'Preferences' -> 'Tools' -> 'Python Integrated Tools' and choose pytest as default test runner. Apply changes.
* Right-click a test and choose 'Run'

### Run tests in CLI ###
* Open Terminal and activate virtualenv (if deactivated)
* Run a test suite: `$ python -m pytest <path/to/test/suite>`. Example: `$ python -m pytest tests/test_petstore.py`