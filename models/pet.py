class Pet:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.category = kwargs.get('category')
        self.name = kwargs.get('name')
        self.photoUrls = kwargs.get('photoUrls')
        self.status = kwargs.get('status')
        self.tags = kwargs.get('tags')

