import uuid

import requests

from config import BASE_URI
from enums import PetStatus
from headers import DEFAULT_HEADERS
from models.pet import Pet
from schemas.pet_schema import PetSchema


def get_pets_by_status(status):
    return requests.get(f'{BASE_URI}/pet/findByStatus', headers=DEFAULT_HEADERS, params={'status': status.value})


def get_and_load_pets_by_status(status):
    res = get_pets_by_status(status)
    return res, PetSchema(many=True).load(res.json())


def create_new_pet():

    new_pet = Pet(**{
        'category': {'id': 1, 'name': f'string {str(uuid.uuid4())}'},
        'name': f'name {str(uuid.uuid4())}',
        'photoUrls': [f'https://foobar.com/{str(uuid.uuid4())}'],
        'tags': [{'id': 1, 'name': f'string {str(uuid.uuid4())}'}],
        'status': PetStatus.AVAILABLE.value
    })
    new_user_json = PetSchema(exclude=['id']).dumps(new_pet)

    return new_pet, requests.post(url=f'{BASE_URI}/pet', data=new_user_json, headers=DEFAULT_HEADERS)


def create_and_load_new_pet():
    new_pet, res = create_new_pet()
    return new_pet, res, PetSchema().load(res.json())


def update_pet(pet_id, exclude_props, **kwargs):
    request_body_params = {
        'id': pet_id,
    }
    request_body_params.update(**kwargs)

    update_pet_obj = Pet(**request_body_params)
    update_pet_json = PetSchema(exclude=exclude_props).dumps(update_pet_obj)

    return update_pet_obj, requests.put(url=f'{BASE_URI}/pet', data=update_pet_json, headers=DEFAULT_HEADERS)


def update_and_load_pet(pet_id, exclude_props, **kwargs):
    updated_pet, res = update_pet(pet_id, exclude_props, **kwargs)
    return updated_pet, res, PetSchema().load(res.json())


def delete_pet(pet_id):
    return requests.delete(url=f'{BASE_URI}/pet/{pet_id}', headers=DEFAULT_HEADERS)