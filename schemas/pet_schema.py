import sys

from marshmallow import Schema, fields, validate, post_load, post_dump

from enums import PetStatus
from models.pet import Pet


class CategorySchema(Schema):
    id = fields.Int(required=True)
    name = fields.Str()


class TagSchema(Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)


class PetSchema(Schema):
    id = fields.Int(required=True)
    category = fields.Nested(CategorySchema)
    name = fields.Str()
    photoUrls = fields.List(fields.Str())
    tags = fields.List(fields.Nested(TagSchema))
    status = fields.Str(validate=validate.OneOf([st.value for st in PetStatus]))

    @post_load
    def make_pet(self, data, **kwargs):
        return Pet(**data)


