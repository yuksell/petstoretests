import time

from assertpy import assert_that

from enums import PetStatus
from request_libraries.pet_library import get_and_load_pets_by_status, create_and_load_new_pet, \
    update_and_load_pet, delete_pet


def test_get_available_pets():
    res, pets_available = get_and_load_pets_by_status(status=PetStatus.AVAILABLE)
    assert_that(res.status_code).is_equal_to(200)
    assert_that(pets_available).extracting('status')\
        .contains(PetStatus.AVAILABLE.value)\
        .does_not_contain(PetStatus.SOLD.value, PetStatus.PENDING.value)


def test_create_new_pet():

    # Create pet with status 'available'
    new_pet, res, pet_returned = create_and_load_new_pet()
    assert_that(res.status_code).is_equal_to(200)
    assert_that([pet_returned])\
        .extracting('category', 'name', 'photoUrls', 'tags', 'status')\
        .contains((new_pet.category, new_pet.name, new_pet.photoUrls, new_pet.tags, new_pet.status))

    # Check pet added. Retry 20 times and wait for 1 second between each retry, because the newly added item does
    # not appear immediately sometimes

    for i in range(20):
        print(f'Trying to find newly created pet in list of available pets. {i+1} of 20')
        res, pets_available = get_and_load_pets_by_status(status=PetStatus.AVAILABLE)
        assert_that(res.status_code).is_equal_to(200)

        try:
            assert_that(pets_available)\
                .extracting('id', 'category', 'name', 'photoUrls', 'tags', 'status')\
                .contains((pet_returned.id, pet_returned.category, pet_returned.name,
                           pet_returned.photoUrls, pet_returned.tags, pet_returned.status))
            break
        except AssertionError as ae:
            if i == 19:
                raise ae
            time.sleep(1)


def test_update_created_pet():

    # Create pet with status 'available'
    new_pet, res, pet_returned = create_and_load_new_pet()
    assert_that(res.status_code).is_equal_to(200)
    assert_that([pet_returned]) \
        .extracting('category', 'name', 'photoUrls', 'tags', 'status') \
        .contains((new_pet.category, new_pet.name, new_pet.photoUrls, new_pet.tags, new_pet.status))

    # Update pet status to "sold" and check status is updated
    updated_pet, res, updated_pet_returned = update_and_load_pet(
                                     pet_id=pet_returned.id,
                                     exclude_props=[],
                                     status=PetStatus.SOLD.value,
                                     category=pet_returned.category,
                                     name=pet_returned.name,
                                     photoUrls=pet_returned.photoUrls,
                                     tags=pet_returned.tags)
    assert_that(res.status_code).is_equal_to(200)
    assert_that([updated_pet_returned]) \
        .extracting('id', 'category', 'name', 'photoUrls', 'tags', 'status') \
        .contains((pet_returned.id, pet_returned.category, pet_returned.name,
                   pet_returned.photoUrls, pet_returned.tags, PetStatus.SOLD.value))


def test_delete_created_updated_pet():

    # Create pet with status 'available'
    new_pet, res, pet_returned = create_and_load_new_pet()
    assert_that(res.status_code).is_equal_to(200)
    assert_that([pet_returned]) \
        .extracting('category', 'name', 'photoUrls', 'tags', 'status') \
        .contains((new_pet.category, new_pet.name, new_pet.photoUrls, new_pet.tags, new_pet.status))

    # Update pet status to "sold" and check status is updated
    updated_pet, res, updated_pet_returned = update_and_load_pet(
        pet_id=pet_returned.id,
        exclude_props=[],
        status=PetStatus.SOLD.value,
        category=pet_returned.category,
        name=pet_returned.name,
        photoUrls=pet_returned.photoUrls,
        tags=pet_returned.tags)
    assert_that(res.status_code).is_equal_to(200)
    assert_that([updated_pet_returned]) \
        .extracting('id', 'category', 'name', 'photoUrls', 'tags', 'status') \
        .contains((pet_returned.id, pet_returned.category, pet_returned.name,
                   pet_returned.photoUrls, pet_returned.tags, PetStatus.SOLD.value))

    # Delete pet and check if it is deleted
    # Delete request has an issue - returns 404 every first time even if the pet id to be deleted exists
    # That's why a retry mechanism is implemented

    delete_res = None

    for i in range(5):
        try:
            delete_res = delete_pet(pet_returned.id)
            assert_that(delete_res.status_code).is_equal_to(200)
            break
        except AssertionError as ae:
            if i == 4:
                raise ae
            time.sleep(1)

    assert_that(delete_res.json()).has_code(200)



